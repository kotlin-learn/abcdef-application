package com.pakawat.abcdefapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.pakawat.abcdefapplication.databinding.FragmentDBinding


class D_Fragment : Fragment() {
    private var _binding  : FragmentDBinding? = null
    private val binding get() = _binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDBinding.inflate(inflater,container,false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.buttonGoC?.setOnClickListener {
            val action = D_FragmentDirections.actionDFragmentToCFragment(letter = "C")
            view.findNavController().navigate(action)
        }
        binding?.buttonGoE?.setOnClickListener {
            val action = D_FragmentDirections.actionDFragmentToEFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}