package com.pakawat.abcdefapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.pakawat.abcdefapplication.B_FragmentDirections.Companion.actionBFragmentToAFragment
import com.pakawat.abcdefapplication.databinding.FragmentBBinding


class B_Fragment : Fragment() {

    var _binding: FragmentBBinding? = null
    private val binding get() = _binding
    lateinit var letter :  String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        letter = arguments?.getString(LETTER).toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBBinding.inflate(inflater,container,false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.buttonGoA?.setOnClickListener{
            val action = B_FragmentDirections.actionBFragmentToAFragment()
            view.findNavController().navigate(action)
        }
        binding?.buttonGoC?.setOnClickListener {
            val letterStr = binding?.buttonGoC?.text.toString()
            val action = B_FragmentDirections.actionBFragmentToCFragment(letterStr)
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    companion object{
        const val LETTER = "letter"
    }

}