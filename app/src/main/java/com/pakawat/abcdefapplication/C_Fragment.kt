package com.pakawat.abcdefapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.pakawat.abcdefapplication.databinding.FragmentCBinding


class C_Fragment : Fragment() {
    private lateinit var letter : String
    private var _binding: FragmentCBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        letter = arguments?.getString(LETTER).toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCBinding.inflate(inflater,container,false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.buttonGoB?.setOnClickListener{
            var letterStr = binding?.buttonGoB?.text.toString()
            val  action = C_FragmentDirections.actionCFragmentToBFragment(letterStr)
            view.findNavController().navigate(action)
        }
        binding?.buttonGoD?.setOnClickListener{
            val  action = C_FragmentDirections.actionCFragmentToDFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding=null
    }

    companion object{
        const val LETTER = "letter"
    }
}