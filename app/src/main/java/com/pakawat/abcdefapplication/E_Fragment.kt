package com.pakawat.abcdefapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.pakawat.abcdefapplication.databinding.FragmentEBinding


class E_Fragment : Fragment() {

    private var _binding : FragmentEBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEBinding.inflate(inflater,container,
        false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.buttonGoD?.setOnClickListener {
            val action = E_FragmentDirections.actionEFragmentToDFragment()
            view.findNavController().navigate(action)
        }

        binding?.buttonGoF?.setOnClickListener {
            val action = E_FragmentDirections.actionEFragmentToFFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding=null
    }
}